package com.zendolin.pokedex;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by valmy on 22/02/2018.
 */

public class DataBase extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "pokemon.db";
    private static final int DATABASE_VERSION = 1;

    public DataBase(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public Cursor getPokemon(){
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String sqlSelect[] = {"numNat, nomFr"};
        String sqlTables = "Pokemon";

        qb.setTables(sqlTables);
        Cursor c = qb.query(db, sqlSelect, null, null,
                null, null, null);


        Log.d("Cursor", String.valueOf(c.getCount()));
        return c;

    }

    public Cursor getDetails(int numNat){
        SQLiteDatabase db = getReadableDatabase();


        Cursor c = db.rawQuery("SELECT nomFr, famille, taille, poids FROM Pokemon WHERE numNat = ?", new String[]{String.valueOf(numNat)});


        Log.d("Cursor", String.valueOf(c.getCount()));
        return c;
    }

    public ArrayList<String> getType(int numNat){
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("SELECT Type.nom FROM Pokemon INNER JOIN TYPE ON Pokemon.numType1 = Type.num WHERE Pokemon.numNat = ? union SELECT Type.nom FROM Pokemon INNER JOIN TYPE ON Pokemon.numType2 = Type.num WHERE Pokemon.numNat = ?", new String[]{String.valueOf(numNat), String.valueOf(numNat)});

        ArrayList<String> type = new ArrayList<String>();

        c.moveToFirst();

        do{
            type.add(c.getString(0));
        }while(c.moveToNext());

        Log.d("Cursor", c.toString());
        return type;

    }
}
