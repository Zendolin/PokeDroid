package com.zendolin.pokedex;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by valmy on 27/02/2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Holder> {

    private HashMap<String, Integer> mItems;
    private Context context;

    MyAdapter(HashMap<String, Integer> items, Context context){
        mItems = items;
        this.context = context;
    }

    public static class Holder extends RecyclerView.ViewHolder{
        private ImageView iv;
        private TextView tv;
        private myClickListener mcl;

        public Holder(View view, myClickListener mcl){
            super(view);

            iv = (ImageView)view.findViewById(R.id.imageView);
            tv = (TextView)view.findViewById(R.id.textView);
            this.mcl = mcl;
            view.setOnClickListener(mcl);
        }

        public myClickListener getMyClickListener(){
            return mcl;
        }

    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowpokemon, parent, false);
        return new Holder(view, new myClickListener(context));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        String key = (String) mItems.keySet().toArray()[position];
        holder.tv.setText(key);
        holder.iv.setImageResource(context.getResources().getIdentifier("miniature_" + mItems.get(key), "drawable", context.getPackageName()));
        holder.getMyClickListener().setNumNat(mItems.get(key));

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private class myClickListener implements View.OnClickListener {
        private int numNat;
        private Context context;


        public myClickListener(Context context){
            super();
            this.context = context;
        }

        @Override
        public void onClick(View view) {
            Log.d("onClickList", "Num nat = " + numNat);
            Intent intent = new Intent(context, PokemonDetail.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("numNat", numNat);
            context.startActivity(intent);
        }

        public void setNumNat(int numNat){
            this.numNat = numNat;
        }
    };
}
