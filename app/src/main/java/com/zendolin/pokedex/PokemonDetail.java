package com.zendolin.pokedex;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;

public class PokemonDetail extends AppCompatActivity {
    private DataBase db;
    private int numNat;
    private TextView nomFr;
    private TextView famille;
    private TextView taille;
    private  TextView poids;
    private GifImageView sprite;
    private ImageView type1;
    private  ImageView type2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_detail);

        db = new DataBase(this);

        numNat = getIntent().getIntExtra("numNat", 1);

        assignation();

        getSetDetails();

    }

    public void assignation(){
        nomFr = (TextView)findViewById(R.id.nomFr);
        famille = (TextView)findViewById(R.id.famille);
        taille = (TextView)findViewById(R.id.taille);
        poids = (TextView)findViewById(R.id.poids);
        sprite = (GifImageView)findViewById(R.id.sprite);
        type1 = (ImageView)findViewById(R.id.type1);
        type2 = (ImageView)findViewById(R.id.type2);
    }

    public void getSetDetails(){
        Cursor c = db.getDetails(numNat);


        c.moveToFirst();

        nomFr.setText(c.getString(0));
        famille.setText("Famille : " + c.getString(1));
        taille.setText(c.getString(2) + "m");
        poids.setText(c.getString(3) + "kg");
        sprite.setImageResource(getResources().getIdentifier("sprite_" + numNat, "drawable", getPackageName()));

        ArrayList<String> type = new ArrayList<String>();
        type = db.getType(numNat);

        if(type.size() == 1){
            type1.setImageResource(getResources().getIdentifier("type_" + type.get(0).toLowerCase(), "drawable", getPackageName()));
            type2.setVisibility(View.INVISIBLE);
        }
        else if(type.size() == 2){
            type1.setImageResource(getResources().getIdentifier("type_" + type.get(0).toLowerCase(), "drawable", getPackageName()));
            type2.setImageResource(getResources().getIdentifier("type_" + type.get(1).toLowerCase(), "drawable", getPackageName()));
        }

    }


}
