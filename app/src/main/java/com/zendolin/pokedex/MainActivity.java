package com.zendolin.pokedex;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private DataBase db;
    private Cursor c;


    private HashMap<String, Integer> pokemon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DataBase(this);
        c = db.getPokemon();

        c.moveToFirst();

        pokemon = new HashMap<>();

        do {
            //Log.d("testDb", c.getString(0));
            pokemon.put(c.getString(1), c.getInt(0));
        }while(c.moveToNext());


        Log.d("HM", pokemon.toString());

        //Recycler View
        RecyclerView rv = findViewById(R.id.listPok);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);



        MyAdapter ma = new MyAdapter(pokemon, getApplicationContext());

        rv.setAdapter(ma);




    }




}
